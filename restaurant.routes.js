const Joi = require('joi')
const { Router } = require('express')
const RestaurantCtrl = require('./restaurants.control.js')
const router = Router()

const validateBody = schema => async (req,res,next) => {
   const { value, error } = schema.validate(req.body, {
      stripUnknown: true,
   })
   if (error) {
      console.log(error.message)
      return res.status(400).send(error)
   }
   req.body = value
   next()
}

const validateQuery = schema => async (req,res,next) => {
   const { value, error } = schema.validate(req.query, {
      stripUnknown: true,
   })
   if (error) {
      console.log(error.message)
      return res.status(400).send(error)
   }
   req.query = value
   next()
}


const boroughDTO = Joi.object({
   borough: Joi.string().required()
})

const cuisineDTO = Joi.object({
   borough: Joi.string().required(),
   cuisine: Joi.string().required()
})

const queryDTO = Joi.object({
   page: Joi.number().empty(),
   perPage: Joi.number().empty()
})

router.get(
   '/restaurants/borough',
   validateBody(boroughDTO),
   RestaurantCtrl.getBorough
)

router.get(
   '/restaurants/cuisine',
   validateBody(cuisineDTO),
   RestaurantCtrl.getCuisines
)

router.get(
   '/restaurants/:id',
   // validate(boroughDTO),
   RestaurantCtrl.getRestaurant
)

router.get(
   '/restaurants',
   validateQuery(queryDTO),
   RestaurantCtrl.getRestaurants
)

module.exports = router
