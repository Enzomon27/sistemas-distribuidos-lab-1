const { Schema, model } = require('mongoose')

const AddressSchema = new Schema({
   building: String,
   coord: Array,
   street: String,
   zipcode: String
},{
   _id: false
})

const RestaurantSchema = new Schema({
   _id: String,
   address: AddressSchema,
   borough: String,
   cuisine: String,
   name: String,
}, {
   colecction: 'restaurant',
   versionKey: false
})

module.exports = model('restaurant',RestaurantSchema)
