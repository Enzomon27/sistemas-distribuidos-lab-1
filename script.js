const RestaurantModel = require('./models/restaurant.model')
const db = require('./db')
const json = require('./restaurants.json')

db();

(async () => {
   const documents = json.map(js => {
      return {
         ...js,
         _id: js.restaurant_id
      }
   })
   /*
   await Promise.all(documents.map(async document => {
      await RestaurantModel.create(document)
      console.log('> Creado restaurant con id: ',document._id)
   }))
   */
   console.log('> Process over')
   console.log('> NUMERO DE DOCUMENTOS: ',json.length)
   return 'Done'
})()
