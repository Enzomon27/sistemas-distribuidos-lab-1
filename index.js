const express = require('express')
const cors = require('cors')
const app = express()
const port = 3200
const db = require('./db')
const RestaurantRouter = require('./restaurant.routes')
const jsonParser = express.json()

app.use(jsonParser)
app.use(cors())

db()

app.use('/api',RestaurantRouter)

app.listen(port,() => {
   console.log(`Listen at port ${port}`)
})
