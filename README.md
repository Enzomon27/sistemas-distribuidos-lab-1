# sistemas-distribuidos-lab-1

Laboratorio 1 para el curso de Sistemas distribuidos

## Requisitos
node v14

## Scripts
Instalar los módulos
```
npm install
```
Ejecutar localmente
```
npm run local
```
Modo desarrollo
```
npm run dev
```
