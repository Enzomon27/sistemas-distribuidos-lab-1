const mongoose = require('mongoose')

module.exports = async () => {
   try {
      await mongoose.connect( 'mongodb://localhost:27017/test')
      return mongoose.connection
   } catch(error) {
      console.error(error.message,error.stack)
      return error
   }
}
