const RestaurantModel = require('./models/restaurant.model')

exports.getBorough = async (req,res) => {
   try {
      console.log('Get by borough')
      const borough = req.body.borough
      const documents = await RestaurantModel.find({
         borough: {
            $regex: `^.*${borough}.*$`,
            $options: 'i'
         }
      }, 'name cuisine borough -_id')
      const total = await RestaurantModel.count({
         borough: {
            $regex: `^.*${borough}.*$`,
            $options: 'i'
         }
      })
      return res.status(200).json({
         total,
         data: documents
      })
   } catch(error) {
      return res.status(500).send({
         message: error.message,
         stack: error.stack
      })
   }
}

exports.getRestaurant = async (req,res) => {
   console.log('Getting restaurant by id')
   try {
      const id = String(req.params.id)
      const restaurant = await RestaurantModel.findById(id)
      if(!restaurant) {
         return res.status(404).json({
            error: {
               message: 'No se encontró ese restaurante'
            }
         })
      }
      return res.status(200).json(restaurant)
   } catch(error) {
      console.error(error.message,error.stack)
      return res.status(500).json({
         error: {
            message: error.message,
            stack: error.stack
         }
      })
   }
}

exports.getCuisines = async (req,res) => {
   console.log('Get Cuisines')
   try {
      const { borough, cuisine } = req.body
      const restaurants = await RestaurantModel.find({
         $and: [{
            borough: {
               $regex: `^.*${borough}.*$`,
               $options: 'i'
            },
            cuisine: {
               $regex: `^.*${cuisine}.*$`,
               $options: 'i'
            }
         }]
      })
      const total = await RestaurantModel.count({
         $and: [{
            borough: {
               $regex: `^.*${borough}.*$`,
               $options: 'i'
            },
            cuisine: {
               $regex: `^.*${cuisine}.*$`,
               $options: 'i'
            }
         }]
      })

      return res.status(200).json({
         total,
         data: restaurants
      })
   } catch(error) {
      return res.status(500).json({
         error: {
            message: error.message,
            stack: error.stack
         }
      })
   }
}

exports.getRestaurants = async (req,res) => {
   console.log('Get Restaurantes paginated')
   try {
      const {
         page = 1,
         perPage = 10,
      } = req.query
      const restaurants = await RestaurantModel
         .find({})
         .skip(perPage*(page-1))
         .limit(perPage)
      const total = await RestaurantModel.count({})
      const pages = Math.ceil(total/perPage)

      return res.status(200).json({
         total,
         data: restaurants,
         pages,
         page,
         perPage
      })
   } catch(error) {
      return res.status(500).json({
         error: {
            message: error.message,
            stack: error.stack
         }
      })
   }
}
